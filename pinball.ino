#pragma once

#ifdef TEST_HW_SEPARATED
#include "HWTests/TestHWSeparated.h"
#endif

#ifdef TEST_HW_LOOP
#include "HWTests/TestHWLoop.h"
#endif

#ifdef TEST_HW_PINBALL
#include "HWTests/TestPinball.h"

#endif

#ifdef TEST_HW_SYNCHRONIZER
#include "HWTests/TestHWSynchronizer.h"
#endif

#ifdef TEST_HW_MYDISPLAY
#include "HWTests/TestHWMyDisplay.h"
#endif
