#pragma once

//Relay:
const int relayPin = 6;

//Buttons & Targets

const int startButtonPin = 2;
const int actionButtonPin = 4;

const int target1ButtonPin = A0;
const int target2ButtonPin = A1;
const int target3ButtonPin = A2;
const int target4ButtonPin = A3;
const int target5ButtonPin = A4;

//LEDs:

constexpr int ledDataPin = 3;

//Displays:

const int commonDisplayDataInPin = 11;   //Fixed
const int commonDisplayClockPin = 13;    //Fixed

const int scoreDisplayCSPin = 7;
const int initialsDisplayCSPin = 8;
const int countdownDisplayCSPin = 10;
const int highScoreDisplayCSPin = 12;

//Audio:
const int audioPin = 9; //Fixed

//Extra: 
const int extraGndPin = 5;