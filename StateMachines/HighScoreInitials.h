#pragma once

#include "../Utils/ISynchronizer.h"
#include "../IOs/IButton.h"
#include "../Display/IDisplay.h"

#include "IHighScoreInitials.h"

template <int NbInitials>
class HighScoreInitialsBase : public IHighScoreInitials
{
    public:
        HighScoreInitialsBase(IButton & buttonNext, IButton & buttonChange, IDisplay & display, IPeriodicSynchronizer & sync)
        : buttonNext(buttonNext), buttonChange(buttonChange), sync(sync), display(display)
        {
            for(int i = 0; i < NbInitials; i++)
                txt[i] = ' ';
            
            txt[NbInitials] = '\0';
        }

        void step()
        {
            if(!sync.sync() || (pos >= NbInitials))
                return;

            if(buttonNext.isPressedDebounced())
            {
                pos++;
                return;
            }

            if(buttonChange.isPressedDebounced())
            {
                txt[pos] = (txt[pos] == ' ' ? 'A' : txt[pos] + 1);

                if(txt[pos] > 'Z')
                    txt[pos] = ' ';

                display.print(txt);
            }
        }

        void start()
        {
            sync.trigger();
            txt[0] = 'A';
            txt[1] = ' ';
            txt[2] = ' ';
            display.print(txt);
        }

        bool isFinished()
        {
            return pos >= NbInitials;
        }

        const char* getInitials()
        {
            return txt;
        }

    private:

        char txt[NbInitials+1];
        int pos = 0;
        
        IPeriodicSynchronizer & sync;
        IButton & buttonNext;
        IButton & buttonChange;
        IDisplay & display;
};

using HighScoreInitials = HighScoreInitialsBase<3>;