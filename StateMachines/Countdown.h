#pragma once

#include "../Utils/ISynchronizer.h"

#include "ICountdown.h"
#include "../Utils/BCD.h"

class Countdown : public ICountdown
{
    public:
        Countdown(BCD4 resetCount, IPeriodicSynchronizer & sync) : resetCount(resetCount), sync(sync)
        {
            sync.setPeriodUs(1000000);
        }

        void start()
        {
            count = resetCount;
            sync.trigger();
        }

        void step()
        {
            if(!sync.sync())
                return;
            
            if(count > BCD4(0))
                count --;
        }

        BCD4 getCount()
        {
            return count;
        }

        bool isFinished()
        {
            return count <= BCD4(0);
        }

    private:
        IPeriodicSynchronizer & sync;
        BCD4 count;
        BCD4 resetCount;
};