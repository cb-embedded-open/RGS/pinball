#pragma once

#include "../IOs/IRelay.h"
#include "../IOs/IButton.h"
#include "../IOs/ILEDs.h"

#include "../Audio/IBeep.h"

#include "../Utils/ISynchronizer.h"

#include "../Display/IDisplay.h"

#include "IHighScoreInitials.h"
#include "IAttractMode.h"
#include "ICountdown.h"
#include "ITargets.h"
#include "../Memory/IScoreMemorizer.h"

#include "IGamePlay.h"

enum class GameState
{
    INIT,
    IDLE,
    PLAYING,
    END,
    HIGHSCORE
};

class GamePlay : IGamePlay
{
    public:

        GamePlay(   IButton & startButton,
                    IRelay & relay,
                    IAttractMode & attractMode,
                    ICountdown & countdown,
                    IHighScoreInitials & highScoreInitials,
                    IDisplay & countdownDisplay,
                    ITargets & targets,
                    IScoreMemorizer & scoreMemorizer
                )
        : 
        startButton(startButton),
        relay(relay),
        attractMode(attractMode),
        countdown(countdown),
        highScoreInitials(highScoreInitials),
        countdownDisplay(countdownDisplay),
        targets(targets),
        scoreMemorizer(scoreMemorizer),
        highScore(highScore),
        score(score),
        initials(initials)
        {
        }

        void step()
        {
            switch(state)
            {
                case GameState::INIT:
                {
                    initState();
                    break;
                }
                case GameState::IDLE:
                {
                    idleState();
                    break;
                }
                case GameState::PLAYING:
                {
                    playingState();
                    break;
                }
                case GameState::END:
                { 
                    endState();
                    break;
                }
                case GameState::HIGHSCORE:
                { 
                    highScoreState();
                    break;
                }
            }
        }

        void reset()
        {
            setState(GameState::INIT);
        }

    private:

        void setState(GameState newState)
        {
            state = newState;
        }

        void initState()
        {
            idleTransition();
        }


        void idleState()
        {
            attractMode.step();

            if(startButton.isPressed())
                playingTransition();
        }

        void playingState()
        {
            countdown.step();

            countdownDisplay.print(countdown.getCount().c_str());
            targets.step();

            if(countdown.isFinished())
                endTransition();
        }

        void endState()
        {
            BCD currentScore = targets.getBCDScore();

            if((currentScore > highScore))
            {
                highScoreTransition();
            }
            else
            {
                idleTransition();
            }
        }

        void highScoreState()
        {
            highScoreInitials.step();

            if(highScoreInitials.isFinished())
            {
                highScoreToIdleTransition();
            }
        }

        void idleTransition()
        {
            setState(GameState::IDLE);
            attractMode.trigger();
        }

        void highScoreToIdleTransition()
        {
            scoreMemorizer.setScore(targets.getBCDScore());
            scoreMemorizer.setInitials(highScoreInitials.getInitials());
            idleTransition();
        }

        void playingTransition()
        {
            setState(GameState::PLAYING);
            attractMode.stop();

            relay.on();
            countdown.start();
        }

        void endTransition()
        {
            setState(GameState::END);
            relay.off();
        }

        void highScoreTransition()
        {
            setState(GameState::HIGHSCORE);
            highScore = targets.getBCDScore();
            highScoreInitials.start();
        }

        GameState state = GameState::IDLE;
        
        BCD highScore = BCD(0);
        BCD score = BCD(0);
        char * initials;

        IButton & startButton;
        IRelay & relay;

        IAttractMode & attractMode;
        ICountdown & countdown;
        IDisplay & countdownDisplay;
        IHighScoreInitials & highScoreInitials;
        ITargets & targets;

        IScoreMemorizer & scoreMemorizer;

};