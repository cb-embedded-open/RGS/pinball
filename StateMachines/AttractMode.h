#pragma once

#include "../Utils/ISynchronizer.h"
#include "../IOs/ILEDs.h"
#include "../Display/IDisplay.h"
#include "IAttractMode.h"
#include "../Utils/BCD.h"

class AttractMode : public IAttractMode
{
    public:

        AttractMode(
                        ILEDs & leds,
                        IPeriodicSynchronizer & sync,
                        IDisplay & countdownDisplay,
                        IDisplay & InitialsDisplay,
                        IDisplay & scoreDisplay,
                        IDisplay & highScoreDisplay,
                        BCD & highScore,
                        BCD & score,
                        char * highScoreInitials
                    )
        :
            leds(leds),
            state(0),
            sync(sync),
            countdownDisplay(countdownDisplay),
            InitialsDisplay(InitialsDisplay),
            scoreDisplay(scoreDisplay),
            highScoreDisplay(highScoreDisplay),
            initialScrollingTextOffset(- 8*9),
            highScore(highScore),
            score(score),
            highScoreInitials(highScoreInitials)
        {
            sync.setPeriodUs(100000);
        }

        void step() override
        {
            if(!sync.sync())
                return;
         
            leds.setRGB(state, 0,0,0);
            state = (state + 1) % 8;
            leds.setRGB(state, 0, 0, 255);

            scrollingText();
        }

        void trigger() override
        {
            sync.trigger();
        }

        void stop() override
        {
            sync.stop();
        }

        bool isRunning() override
        {
            return sync.isRunning();
        }

    private:
        IPeriodicSynchronizer & sync;
        ILEDs & leds;
        int state;

        BCD & highScore;
        BCD & score;
        char * highScoreInitials;

        IDisplay & countdownDisplay;
        IDisplay & InitialsDisplay;
        IDisplay & scoreDisplay;
        IDisplay & highScoreDisplay;

        const int16_t initialScrollingTextOffset;
        int16_t scrollingTextOffset = initialScrollingTextOffset;

        void scrollingText()
        {
            const char text[] = "TROLLBALL THE PINBALL GAME";

            countdownDisplay.print(text, scrollingTextOffset);
            InitialsDisplay.print(text, scrollingTextOffset + 8*5);

            scoreDisplay.print(score.c_str());
            highScoreDisplay.print(highScore.c_str());

            scrollingTextOffset++;
            if(scrollingTextOffset > ((int16_t) sizeof(text)*8))
                scrollingTextOffset = initialScrollingTextOffset;
        }
};