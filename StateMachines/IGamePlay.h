#pragma once

class IGamePlay
{
    public:
        virtual void step() = 0;
        virtual void reset() = 0;
};