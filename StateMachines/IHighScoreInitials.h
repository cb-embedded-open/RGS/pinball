#pragma once

class IHighScoreInitials
{
    public:
        virtual void step() = 0;
        virtual void start() = 0;
        virtual bool isFinished() = 0;
        virtual const char* getInitials() = 0;
};
