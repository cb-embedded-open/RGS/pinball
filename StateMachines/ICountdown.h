#pragma once

#include "../Utils/BCD.h"

class ICountdown
{
    public:
        virtual void start() = 0;
        virtual void step() = 0;
        virtual BCD4 getCount() = 0;
        virtual bool isFinished() = 0;
};