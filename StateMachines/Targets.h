#pragma once

#include "../IOs/IButton.h"
#include "../Audio/IBeep.h"
#include "../Utils/BCD.h"
#include "../Display/IDisplay.h"
#include "ITargets.h"

#include "../Indicators/IIndicators.h"

class Targets : public ITargets
{
    public:

    static constexpr int NbTargets = 5;
    static constexpr int goalTarget = (NbTargets/2)+1;
    
    Targets(
                IButton * targets[NbTargets],
                IBeep & beep,
                IDisplay & scoreDisplay,
                IIndicators & targetIndicators,
                IIndicators & bonusIndicators
            )
    : 
                beep(beep),
                scoreDisplay(scoreDisplay),
                bonusIndicators(bonusIndicators),
                targetIndicators(targetIndicators),
                bonusCount(0)
    {
        for (int i = 0; i < NbTargets; i++)
            this->targets[i] = targets[i];

        reset();
    }

    void step()
    {
        for (int i = 0; i < NbTargets; i++)
        {
            if(isTargetPressed(i))
            {
                onTargetPressed(i);
            }
        }

        if(bonusCondition())
        {
            onBonus();
        }
    }

    void reset()
    {
        bcdScore = BCD(0);
        bcdBonus = BCD(200);
        bonusCount = 0;
        clearFlags();
    }

    BCD getBCDScore() const
    {
        return bcdScore;
    }

    private:

        void onScoreUpdate()
        {
            //Update display
            scoreDisplay.print(getBCDScore().c_str());
        }

        void onTargetPressed(int i)
        {
            bcdScore += isGoalTarget(i) ? BCD(150) : BCD(10);

            onScoreUpdate();

            const int notes[] = { 2*261, 2*294, 2*329, 2*349, 2*392 };

            if(!getFlags(i))
            {
                setFlag(i);
                targetIndicators.set(i);
            }

            beep.beep(notes[i]);
        }

        bool bonusCondition()
        {
            return checkFlags();
        }

        void onBonus()
        {
            targetIndicators.clearAll();
            clearFlags();

            bcdScore += bcdBonus;
            bcdBonus += BCD(200);

            bonusIndicators.set(bonusCount);
            bonusCount++;

            onScoreUpdate();
        }

        bool isGoalTarget(int target)
        {
            return target == goalTarget;
        }

        bool isTargetPressed(int target)
        {
            return targets[target]->isPressedDebounced();
        }

        void setFlag(int target)
        {
            targetFlag[target] = true;
        }

        bool getFlags(int i)
        {
            return targetFlag[i];
        }

        void clearFlags()
        {
            for (int i = 0; i < NbTargets; i++)
                targetFlag[i] = false;
        }

        bool checkFlags()
        {
            for (int i = 0; i < NbTargets; i++)
            {
                if(!targetFlag[i])
                    return false;
            }
            return true;
        }

        //Score / Bonus logic (in BCD format):
        BCD bcdBonus = BCD(200);
        BCD bcdScore = BCD(0);
        int bonusCount = 0;
        
        //Buttons & Flags:
        IButton * targets[NbTargets];
        bool targetFlag[NbTargets] = {false};
        
        //Audio:
        IBeep & beep;

        //Displays:
        IDisplay & scoreDisplay;

        // Indicators (LEDs)
        IIndicators & bonusIndicators;
        IIndicators & targetIndicators;
};