#pragma once

#include "../Utils/BCD.h"

class ITargets
{
    public:
        virtual void step() = 0;
        virtual void reset() = 0;
        virtual BCD getBCDScore() const = 0;
};