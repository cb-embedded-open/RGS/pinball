#pragma once

#include "../Utils/BCD.h"

class IAttractMode
{
    public:
        virtual void step() = 0;
        virtual void trigger() = 0;
        virtual void stop() = 0;
        virtual bool isRunning() = 0;
};