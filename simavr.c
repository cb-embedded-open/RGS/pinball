//#define SIMAVR
#ifdef SIMAVR

#include <avr/io.h>
#include "avr_mcu_section.h"

AVR_MCU(16000000, "atmega328p");

const struct avr_mmcu_vcd_trace_t _mytrace[] _MMCU_ = {
    { AVR_MCU_VCD_SYMBOL("PORTD3_LEDs"), .mask = (1 << 3), .what = (void*)&PORTD, },                // Trace for PD3 (Arduino pin 3)
    { AVR_MCU_VCD_SYMBOL("PORTD7_ScoreDisplayCS"), .mask = (1 << 7), .what = (void*)&PORTD, },      // Trace for PD7 (Arduino pin 7)
    { AVR_MCU_VCD_SYMBOL("PORTB0_InitialsDisplayCS"), .mask = (1 << 0), .what = (void*)&PORTB, },   // Trace for PB0 (Arduino pin 8)
    { AVR_MCU_VCD_SYMBOL("PORTB2_CountdownDisplayCS"), .mask = (1 << 2), .what = (void*)&PORTB, },  // Trace for PB2 (Arduino pin 10)
    { AVR_MCU_VCD_SYMBOL("PORTB4_HighScoreDisplayCS"), .mask = (1 << 4), .what = (void*)&PORTB, },  // Trace for PB4 (Arduino pin 12)
    { AVR_MCU_VCD_SYMBOL("SPI_SPDR"), .mask = 0xFF, .what = (void*)&SPDR, }, // Trace for SPI Data Register
    { AVR_MCU_VCD_SYMBOL("TIMER1_OCR1A"), .mask = 0xFFFF, .what = (void*)&OCR1A, }, // Trace for Timer1 Output Compare Register A
    { AVR_MCU_VCD_SYMBOL("TIMER1_OCR1B"), .mask = 0xFFFF, .what = (void*)&OCR1B, }, // Trace for Timer1 Output Compare Register B
    // { AVR_MCU_VCD_SYMBOL("SPH"), .mask = 0xFF, .what = (void*)&SPH, },
    // { AVR_MCU_VCD_SYMBOL("SPL"), .mask = 0xFF, .what = (void*)&SPL, },
    // { AVR_MCU_VCD_SYMBOL("UDR0"), .what = (void*)&UDR0, },
	// { AVR_MCU_VCD_SYMBOL("UDRE0"), .mask = (1 << UDRE0), .what = (void*)&UCSR0A, },
};

#endif