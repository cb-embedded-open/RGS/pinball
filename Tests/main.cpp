#include "TestCountdown.h"
#include "TestAttractMode.h"
#include "TestHighScoreInitials.h"
#include "TestGamePlay.h"
#include "TestTargets.h"
#include "TestBCD.h"
#include "TestPinball.h"

void unitTests()
{
    testCountdown();
    testAttractMode();
    testHighScoreInitials();
    testGamePlay();
    testTargets();
    testBCD();
}

void integrationTests()
{
    testPinballIntegration();
}

int main()
{
    unitTests();
    integrationTests();
    
    return 0;
}