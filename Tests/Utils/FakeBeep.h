#pragma once

#include "../../Audio/IBeep.h"

class FakeBeep : public IBeep
{
    public:

        FakeBeep()
        {
        }

        void beep(int freq) override
        {
            m_spyCountBeep++;
        }

        void beep() override
        {
            m_spyCountBeep++;
        }

        void step() override
        {
        }

        int getSpyCountBeep()
        {
            return m_spyCountBeep;
        }

        int m_spyCountBeep = 0;
};