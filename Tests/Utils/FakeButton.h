#pragma once

#include "../../IOs/IButton.h"

class FakeButton : public IButton
{
    public:

        FakeButton()
        {
            m_isPressed = false;
        }

        bool isPressed() override
        {
            return m_isPressed;
        }

        bool isPressedDebounced() override
        {
            return isPressed();
        }

        void press()
        {
            m_isPressed = true;
        }

        void release()
        {
            m_isPressed = false;
        }

    private:
        bool m_isPressed = false;
};