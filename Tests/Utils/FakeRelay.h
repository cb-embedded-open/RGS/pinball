#pragma once

#include "../../IOs/IRelay.h"

class FakeRelay : public IRelay
{
    public:

        void on() override
        {
            m_spyCountOn++;
            m_isOn = true;
        }

        void off() override
        {
            m_spyCountOff++;
            m_isOn = false;
        }

        bool isOn()
        {
            return m_isOn;
        }

        int getSpyCountOn()
        {
            return m_spyCountOn;
        }

        int getSpyCountOff()
        {
            return m_spyCountOff;
        }

    private:

        bool m_isOn = false;

        int m_spyCountOn = 0;
        int m_spyCountOff = 0;
};