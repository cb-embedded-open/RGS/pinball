#pragma once

#include "../../StateMachines/ITargets.h"
#include "../../Utils/BCD.h"

class FakeTargets : public ITargets
{
    public:

    void step() override
    {
    }

    void reset() override
    {
    }

    BCD getBCDScore() const override
    {
        return bcdScore;
    }

    void setBCDScore(BCD score)
    {
        bcdScore = score;
    }

    private:
        BCD bcdScore;
};