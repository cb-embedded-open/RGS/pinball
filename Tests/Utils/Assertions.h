#pragma once

#include <iostream>
#include <string>

void printRed(const std::string& message)
{
    std::cout << "\033[1;31m" << message << "\033[0m" << std::endl;
}

void printGreen(const std::string& message)
{
    std::cout << "\033[1;32m" << message << "\033[0m" << std::endl;
}

bool assert(bool condition, const std::string& message)
{
    if (!condition)
    {
        printRed("Test failed: " + message);
    }

    return condition;
}


template <typename T>
bool assertEqual(const T& expected, const T& actual, const std::string& message)
{
    if (expected != actual)
    {
        printRed("Test failed: " + message + " (actual: " + std::to_string(actual) + ", expected: " + std::to_string(expected) + ")");
    }

    return expected == actual;
}

bool assertEqual(const std::string_view & expected,const  std::string_view & actual, const std::string& message)
{
    if (expected != actual)
    {
        printRed("Test failed: " + std::string(message) + " (actual: " + std::string(actual) + ", expected: " + std::string(expected) + ")");
    }

    return expected == actual;
}


void testBanner(const std::string& message)
{
    std::cout << "Running test: " << message << std::endl;
}

void testFooter(bool success)
{
    if (success)
    {
        printGreen("Test passed");
    }
    else
    {
        printRed("Test failed");
    }

    std::cout << std::endl;
}
