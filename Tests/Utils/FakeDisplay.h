#pragma once

#include "../../Display/IDisplay.h"

class FakeDisplay : public IDisplay
{
    public:

        FakeDisplay()
        {
        }

        void print(const char* txt) override
        {
            m_txt = txt;
            printf("Text: %s\n", txt);
        }

        void print(const char* txt, int16_t offset) override
        {
            this->print(txt);
        }

        std::string_view get()
        {
            return std::string_view(m_txt);
        }

        private:
            const char* m_txt = "";
};