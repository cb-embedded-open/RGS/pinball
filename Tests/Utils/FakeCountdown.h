#pragma once

#include "../../StateMachines/ICountdown.h"

class FakeCountdown : public ICountdown
{
    public:

        FakeCountdown()
        {
        }

        void start() override
        {
            m_spyCountStart++;
        }

        void step() override
        {
            m_spyCountStep++;
        }

        BCD4 getCount() override
        {
            return BCD4(m_count);
        }

        bool isFinished() override
        {
            return (m_count == 0);
        }

        void setCount(int count)
        {
            m_count = count;
        }

        private:

        int m_count = 0;

        int m_spyCountStart = 0;
        int m_spyCountStep = 0;
        int m_spyCountGetCount = 0;
};