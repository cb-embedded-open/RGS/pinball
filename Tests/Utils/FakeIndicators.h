#pragma once

#include "../../Indicators/IIndicators.h"

class FakeIndicators : public IIndicators
{
    public:
        virtual void set(int index)
        {
            spySetCount++;
        }

        virtual void clear(int index)
        {
            spyClearCount++;
        }

        virtual void clearAll()
        {
            spyClearAllCount++;
        
        }

        int getSpySetCount()
        {
            return spySetCount;
        }

        int getSpyClearCount()
        {
            return spyClearCount;
        }

        int getSpyClearAllCount()
        {
            return spyClearAllCount;
        }

    private:
        int spySetCount = 0;
        int spyClearCount = 0;
        int spyClearAllCount = 0;
};