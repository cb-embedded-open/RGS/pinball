#pragma once

#include "../../IOs/ILEDs.h"
#include <iostream>
#include <iomanip>

template <int N>
class FakeLEDsBase : public ILEDs
{
    public:
        FakeLEDsBase() : nbLEDs(N), spyCountSetRGB(0)
        {
            for (int i = 0; i < N; i++)
            {
                leds[i][0] = 0;
                leds[i][1] = 0;
                leds[i][2] = 0;
            }
        }

        void setRGB(int index, int r, int g, int b) override
        {
            leds[index][0] = r;
            leds[index][1] = g;
            leds[index][2] = b;

            spyCountSetRGB++;
        }

        void setHSV(int index, int h, int s, int v) override
        {
            // Not implemented
        }

        int getNbLEDs() const override
        {
            return nbLEDs;
        }

        void clearAll() override
        {
            for (int i = 0; i < N; i++)
            {
                leds[i][0] = 0;
                leds[i][1] = 0;
                leds[i][2] = 0;
            }
        }

        void print()
        {
            for (int i = 0; i < N; i++)
            {
                std::cout << std::setw(4) << leds[i][0] << " " << std::setw(4) << leds[i][1] << " " << std::setw(4) << leds[i][2] << " |";
            }

            std::cout << std::endl;
        }

        int getSpyCountSetRGB() const
        {
            return spyCountSetRGB;
        }

        void clearSpyCountSetRGB()
        {
            spyCountSetRGB = 0;
        }

    private:
        int leds[N][3];
        int nbLEDs;

        int spyCountSetRGB = 0;
};

using FakeLEDs = FakeLEDsBase<8>;