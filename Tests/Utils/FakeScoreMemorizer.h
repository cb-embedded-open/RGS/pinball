#pragma once

#include "../../Memory/IScoreMemorizer.h"

class FakeScoreMemorizer : public IScoreMemorizer
{
    public:

        virtual void setScore(BCD score) override
        {
            spySetScore++;
        }

        virtual BCD getScore() override
        {
            spyGetScore++;
            return BCD(1234);
        }

        virtual void setInitials(const char* initials) override
        {
            spySetInitials++;
        }

        virtual const char* getInitials() override
        {
            spyGetInitials++;

            return "ABCD";
        }

        private:

            int spySetScore = 0;
            int spyGetScore = 0;
            int spySetInitials = 0;
            int spyGetInitials = 0;
};