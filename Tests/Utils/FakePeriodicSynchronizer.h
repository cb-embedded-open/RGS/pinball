#pragma once

#include "../../Utils/ISynchronizer.h"

struct FakePeriodicSynchronizer : public IPeriodicSynchronizer
{
        void setPeriodUs(unsigned long periodUs) override
        {
            m_spyCountSetPeriodUs++;
        }

        bool sync() override
        {
            m_spyCountSync++;
            return true;
        }

        void trigger() override
        {
            m_spyCountTrigger++;
        }

        void stop() override
        {
            m_spyCountStop++;
        }

        bool isRunning() override
        {
            m_spyCountIsRunning++;
            return true;
        }

        //Count number of calls of each method:
        int m_spyCountSetPeriodUs = 0;
        int m_spyCountSync = 0;
        int m_spyCountTrigger = 0;
        int m_spyCountStop = 0;
        int m_spyCountIsRunning = 0;
};