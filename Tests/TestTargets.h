#include "Utils/Assertions.h"

#include "Utils/FakeButton.h"
#include "Utils/FakeIndicators.h"
#include "Utils/FakeBeep.h"
#include "Utils/FakeDisplay.h"

#include "../StateMachines/Targets.h"

struct TestTargetsContext
{
    public:
        TestTargetsContext() : targets(
                                        buttonsArray,
                                        beep,
                                        scoreDisplay,
                                        targetIndicators, 
                                        bonusIndicators
                                    )
        {
        }

        FakeButton buttons[Targets::NbTargets];
        IButton * buttonsArray [Targets::NbTargets]= {&buttons[0], &buttons[1], &buttons[2], &buttons[3], &buttons[4]};
        FakeBeep beep;
        FakeIndicators bonusIndicators;
        FakeIndicators targetIndicators;
        Targets targets;
        FakeDisplay scoreDisplay;
};

static void testScoreIncrement()
{
    TestTargetsContext context;

    testBanner("Targets Score increment");

    {
        context.targets.reset();
        if(!assertEqual("0000", context.targets.getBCDScore().c_str(), "Initial score should be 0")) goto footer;
    }

    {
        context.buttons[0].press();
        context.targets.step();
        context.buttons[0].release();

        if(!assertEqual("0010", context.targets.getBCDScore().c_str(), "Score should have incremented by 10")) goto footer;
    }

    {
        context.buttons[0].press();
        context.targets.step();
        context.buttons[0].release();

        if(!assertEqual("0020", context.targets.getBCDScore().c_str(), "Score should have incremented by 10")) goto footer;
    }

    {
        context.buttons[Targets::goalTarget].press();
        context.targets.step();
        context.buttons[Targets::goalTarget].release();

        if(!assertEqual("0170", context.targets.getBCDScore().c_str(), "Goal: Score should have incremented by 150")) goto footer;
    }

    {
        for(int i = 0; i < Targets::NbTargets; i++)
        {
            context.buttons[i].press();
            context.targets.step();
            context.buttons[i].release();
        }

        if(!assertEqual("0560", context.targets.getBCDScore().c_str(), "All targets (regular + Goal): Score should be incremented and bonus applied")) goto footer;

        if(!assertEqual(1, context.bonusIndicators.getSpySetCount(), "All targets (regular + Goal): Bonus LED should have been set exactly once")) goto footer;
    }

    return testFooter(true);

    footer:
        testFooter(false);
}

static void testLEDs()
{
    TestTargetsContext context;

    testBanner("Targets LEDs");

    {
        context.targets.reset();

        for(int i = 0; i < Targets::NbTargets; i++)
        {
            context.buttons[i].press();
            context.targets.step();
            context.buttons[i].release();
        }

        if(!assertEqual(Targets::NbTargets, context.targetIndicators.getSpySetCount(), "All targets: LEDs should have been set")) goto footer;
    }

    return testFooter(true);

    footer:
        testFooter(false);
}

static void testBeep()
{
    TestTargetsContext context;

    testBanner("Targets Beep");

    {
        context.targets.reset();
        for(int i = 0; i < Targets::NbTargets; i++)
        {
            context.buttons[i].press();
            context.targets.step();
            context.buttons[i].release();
        }

        if(!assertEqual(5, context.beep.getSpyCountBeep(), "All targets: Beep should have been called 5 times")) goto footer;
    }

    return testFooter(true);

    footer:
        testFooter(false);
}

void testTargets()
{
    testScoreIncrement();
    testLEDs();
    testBeep();
}