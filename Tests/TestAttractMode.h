#include "../StateMachines/AttractMode.h"
#include "Utils/FakePeriodicSynchronizer.h"
#include "Utils/FakeLEDs.h"
#include "Utils/FakeDisplay.h"

#include "Utils/Assertions.h"

void testAttractMode()
{
    testBanner("AttractMode");

    FakeLEDs leds;
    FakePeriodicSynchronizer sync;
    FakeDisplay countdownDisplay;
    FakeDisplay initialsDisplay;
    FakeDisplay scoreDisplay;
    FakeDisplay highScoreDisplay;

    BCD highScore(4);
    BCD score(4);
    char highScoreInitials[] = "AAAA";

    AttractMode attract(
                            leds,
                            sync,
                            countdownDisplay,
                            initialsDisplay,
                            scoreDisplay,
                            highScoreDisplay,
                            highScore,
                            score,
                            highScoreInitials
                        );

    attract.trigger();

    for (int i = 0; i < 8; i++)
    {
        attract.step();
        leds.print();
    }

    if(!assertEqual(sync.m_spyCountSync, 8, "Synchronizer sync() method should be called exactly 8 times")) goto footer;

    testFooter(true);
    return;

    footer: 
        testFooter(false);
}