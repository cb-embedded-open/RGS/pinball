#include "Utils/Assertions.h"

#include "Utils/FakeButton.h"
#include "Utils/FakeRelay.h"
#include "Utils/FakeLEDs.h"

#include "Utils/FakeDisplay.h"

#include "../StateMachines/GamePlay.h"

#include "../StateMachines/IAttractMode.h"
#include "../StateMachines/IHighScoreInitials.h"
#include "../StateMachines/ICountdown.h"

#include "Utils/FakeTargets.h"
#include "Utils/FakeScoreMemorizer.h"

class FakeAttractMode : public IAttractMode
{
    public:
        void step() override
        {
            m_spyCountStep++;
        }

        void trigger() override
        {
            m_spyCountTrigger++;
        }

        void stop() override
        {
            m_spyCountStop++;
        }

        bool isRunning() override
        {
            m_spyCountIsRunning++;
            return true;
        }

        int getSpyCountStep()
        {
            return m_spyCountStep;
        }

        int getSpyCountTrigger()
        {
            return m_spyCountTrigger;
        }

        int getSpyCountStop()
        {
            return m_spyCountStop;
        }

        int getSpyCountIsRunning()
        {
            return m_spyCountIsRunning;
        }

    private:
        int m_spyCountStep = 0;
        int m_spyCountTrigger = 0;
        int m_spyCountStop = 0;
        int m_spyCountIsRunning = 0;
};

class FakeHighScoreInitials : public IHighScoreInitials
{
    public:
        void step() override
        {
            m_spyCountStep++;
        }

        void start() override
        {
            m_spyCountStart++;
        }

        bool isFinished() override
        {
            m_spyCountIsFinished++;
            return m_isFinished;
        }

        void finish()
        {
            m_isFinished = true;
        }

        int getSpyCountStep()
        {
            return m_spyCountStep;
        }

        int getSpyCountStart()
        {
            return m_spyCountStart;
        }

        int getSpyCountIsFinished()
        {
            return m_spyCountIsFinished;
        }

        const char* getInitials() override
        {
            return "ABCD";
        }

    private:
        int m_spyCountStep = 0;
        int m_spyCountStart = 0;
        int m_spyCountIsFinished = 0;

        bool m_isFinished = false;
};

class FakeCountdown : public ICountdown
{
    public:
        void start() override
        {
        }

        void step() override
        {
        }

        BCD4 getCount() override
        {
            return BCD4(0);
        }

        bool isFinished() override
        {
            return m_isFinished;
        }

        void finish()
        {
            m_isFinished = true;
        }

    private:
        bool m_isFinished = false;
};


void testGamePlay()
{
    testBanner("GamePlay");

    FakeButton startButton;
    FakeButton actionButton;

    FakeRelay relay;
    FakeLEDs leds;

    FakeDisplay countdownDisplay;
    FakeDisplay initialsDisplay;

    FakeAttractMode attractMode;
    FakeCountdown countdown;
    FakeHighScoreInitials highScoreInitials;

    FakeTargets targets;

    FakeScoreMemorizer scoreMemorizer;

    GamePlay gamePlay(startButton,
            relay,
            attractMode,
            countdown,
            highScoreInitials,
            countdownDisplay,
            targets,
            scoreMemorizer
        );

    //Attract Mode:
    {
        gamePlay.reset();

        for(int i = 0; i < 10; i++)
        {
            gamePlay.step();
        }

        if(!assert(!relay.isOn(), "Relay should be off at start")) goto footer;
        if(!assertEqual(1, attractMode.getSpyCountTrigger(), "AttractMode should have been triggered exactly once")) goto footer;
        if(!assert(attractMode.getSpyCountStep() > 0, "AttractMode should have been stepped")) goto footer;
    }

    //Press start button:
    {
        startButton.press();

        for(int i = 0; i < 10; i++)
        {
            gamePlay.step();
        }

        startButton.release();


        if(!assert(relay.isOn(), "Relay should be on after pressing start button")) goto footer;
        if(!assertEqual("0000", countdown.getCount().c_str(), "Countdown should not have started")) goto footer;
    }

    //Score:
    {
        targets.setBCDScore(BCD(100));
    }


    //End of countdown:
    {
        countdown.finish();
        printf("Countdown finished\n");

        for(int i = 0; i < 10; i++)
        {
            gamePlay.step();
        }

        if(!assert(!relay.isOn(), "Relay should be off after end of countdown")) goto footer;
        if(!assertEqual(1, highScoreInitials.getSpyCountStart(), "HighScoreInitials should have been started exactly once")) goto footer;
        if(!assert(highScoreInitials.getSpyCountStep() > 0, "HighScoreInitials should have been stepped")) goto footer;
    }

    //End of HighScoreInitials:high
    {
        highScoreInitials.finish();

        int attractModeStepCount = attractMode.getSpyCountStep();
        gamePlay.step(); //Switch to attract mode (not stepped yet)

        //Ten steps in attract mode
        for(int i = 0; i < 10; i++)
        {
            gamePlay.step();
        }

        if(!assertEqual(attractModeStepCount + 10, attractMode.getSpyCountStep(), "AttractMode should have been stepped")) goto footer;
    }

    return testFooter(true);

    footer:
        testFooter(false);
}