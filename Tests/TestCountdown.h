#include "Utils/Assertions.h"

#include "../StateMachines/Countdown.h"
#include "Utils/FakePeriodicSynchronizer.h"

//Test from 5 to 0. Check if the countdown is finished, and if the count is correct.
//Does not go below 0. Check if sync function is called.
void testCountdown()
{
    testBanner("Countdown");

    FakePeriodicSynchronizer sync;
    Countdown countdown(BCD4(5), sync);

    countdown.start();
    if(!assertEqual(countdown.getCount().c_str(), "0005", "Countdown should start at 5")) goto footer;
    if(!assert(!countdown.isFinished(), "Countdown should not be finished")) goto footer;

    countdown.step();
    if(!assertEqual(countdown.getCount().c_str(), "0004", "Countdown should be at 4")) goto footer;

    countdown.step();
    if(!assertEqual(countdown.getCount().c_str(), "0003", "Countdown should be at 3")) goto footer;

    countdown.step();
    if(!assertEqual(countdown.getCount().c_str(), "0002", "Countdown should be at 2")) goto footer;

    countdown.step();
    if(!assertEqual(countdown.getCount().c_str(), "0001", "Countdown should be at 1")) goto footer;

    countdown.step();
    if(!assertEqual(countdown.getCount().c_str(), "0000", "Countdown should be at 0")) goto footer;
    if(!assert(countdown.isFinished(), "Countdown should be finished")) goto footer;

    countdown.step();
    if(!assertEqual(countdown.getCount().c_str(), "0000", "Countdown should not go below 0")) goto footer;
    if(!assert(countdown.isFinished(), "Countdown should be finished")) goto footer;

    if(!assertEqual(sync.m_spyCountTrigger, 1, "Trigger should be called exactly once")) goto footer;
    if(!assertEqual(sync.m_spyCountSync, 6, "Sync should be called exactly 6 times")) goto footer;
    if(!assertEqual(sync.m_spyCountSetPeriodUs, 1, "SetPeriodUs should be called exactly once")) goto footer;
    if(!assertEqual(sync.m_spyCountStop, 0, "Stop should not be called")) goto footer;
    if(!assertEqual(sync.m_spyCountIsRunning, 0, "IsRunning should not be called")) goto footer;

    return testFooter(true);

    footer:
        testFooter(false);
}