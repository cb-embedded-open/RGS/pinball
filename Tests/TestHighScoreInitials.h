#include "Utils/FakePeriodicSynchronizer.h"
#include "Utils/FakeLEDs.h"
#include "Utils/Assertions.h"
#include "Utils/FakeButton.h"
#include "Utils/FakeBeep.h"
#include "Utils/FakeRelay.h"
#include "Utils/FakeDisplay.h"

#include "../StateMachines/HighScoreInitials.h"
#include <string>

void testHighScoreInitials()
{
    testBanner("HighScoreInitials");

    FakeButton buttonNext;
    FakeButton buttonChange;
    FakeDisplay initialsDisplay;
    FakePeriodicSynchronizer sync;

    HighScoreInitials highScoreInitials(
                                            buttonNext,
                                            buttonChange,
                                            initialsDisplay,
                                            sync
                                        );


        highScoreInitials.start();
        highScoreInitials.step();
        highScoreInitials.step();

        //Letter A:
        {
        buttonNext.press();
        highScoreInitials.step();
        }

        //Letter B:
        {
            buttonNext.release();
            buttonChange.press();
            highScoreInitials.step();
            highScoreInitials.step();

            buttonNext.press();
            highScoreInitials.step();
        }

        //Letter C + Check RollOver:
        {
            buttonNext.release();
            buttonChange.press();

            //26 Letters, + 1 for space:
            for(int i = 0; i < (27)*3; i++) //3 Rounds
            {
                highScoreInitials.step();
            }


            highScoreInitials.step();
            highScoreInitials.step();
            highScoreInitials.step();

            buttonNext.press();
            highScoreInitials.step();
        }

        if(!assertEqual(std::string_view("ABC"), initialsDisplay.get(), "Unexpected initials")) goto footer;
        if(!assert(highScoreInitials.isFinished(), "HighScoreInitials should be finished")) goto footer;

    testFooter(true);
    return;

    footer:
    testFooter(false);   
}