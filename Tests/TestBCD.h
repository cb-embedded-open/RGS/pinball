#include "../Utils/BCD.h"
#include "Utils/Assertions.h"

void testBCD()
{
    testBanner("BCD");

    BCD a(1234);
    BCD b(5432);
    BCD c = a + b;

    BCD d(100);
    BCD e(0);

    assertEqual("0000", e.c_str(), "Bad bcdValue construction 0");
    assertEqual("1234", a.c_str(), "Bad bcdValue construction 1");
    assertEqual("5432", b.c_str(), "Bad bcdValue construction 2");
    assertEqual("0100", d.c_str(), "Bad bcdValue construction 3");

    if(!assert(b > a, "b BCD should be greater than a")) goto footer;
    if(!assert(d > e, "d BCD should be greater than e")) goto footer;

    assertEqual("6666", c.c_str(), "BCD addition failed 1234 + 5432 = 6666");

    return testFooter(true);

    footer:
        testFooter(false);
}