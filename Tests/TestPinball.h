#include "../StateMachines/GamePlay.h"
#include "../StateMachines/Targets.h"
#include "../StateMachines/HighScoreInitials.h"
#include "../StateMachines/AttractMode.h"
#include "../StateMachines/Countdown.h"

#include "Utils/FakeButton.h"
#include "Utils/FakeRelay.h"
#include "Utils/FakeDisplay.h"
#include "Utils/FakeIndicators.h"
#include "Utils/FakeLEDs.h"
#include "Utils/FakePeriodicSynchronizer.h"
#include "Utils/FakeBeep.h"
#include "Utils/FakeScoreMemorizer.h"

#include "../Utils/BCD.h"

class FakePinball
{
    public:
        FakePinball():
        targets(
                    targetsArray,
                    beep,
                    scoreDisplay,
                    targetIndicators,
                    bonusIndicators
                ),
        highScoreInitials(
                    startButton,
                    actionButton,
                    initialsDisplay,
                    highScoreSync
                ),
        attractMode(
                    leds,
                    attractSync,
                    countdownDisplay,
                    initialsDisplay,
                    scoreDisplay,
                    highScoreDisplay,
                    highScore,
                    score,
                    initials
                ),
        countdown(
                    BCD4(60),
                    countdownSync
                ),
        scoreMemorizer(),
        gamePlay(
            startButton,
            relay,
            attractMode,
            countdown,
            highScoreInitials,
            countdownDisplay,
            targets,
            scoreMemorizer
        )
        {}

        void step()
        {
            gamePlay.step();
        }
    
        //Fake Components:
        FakeButton startButton;
        FakeButton actionButton;
        FakeButton targetsButtons[5];
        IButton * targetsArray[5] = {&targetsButtons[0], &targetsButtons[1], &targetsButtons[2], &targetsButtons[3], &targetsButtons[4]};

        FakePeriodicSynchronizer highScoreSync;
        FakePeriodicSynchronizer attractSync;
        FakePeriodicSynchronizer countdownSync;

        FakeBeep beep;
        FakeRelay relay;
        FakeScoreMemorizer scoreMemorizer;

        FakeDisplay scoreDisplay;
        FakeDisplay highScoreDisplay;
        FakeDisplay initialsDisplay;
        FakeDisplay countdownDisplay;
        FakeIndicators targetIndicators;
        FakeIndicators bonusIndicators;
        FakeLEDsBase<18> leds;

        //State Machines Under Test:
        GamePlay gamePlay;
        Targets targets;
        HighScoreInitials highScoreInitials;
        AttractMode attractMode;
        Countdown countdown;

        BCD highScore = BCD(5678);
        BCD score = BCD(1234);
        char initials[4] = {'B', 'A', 'B', 'A'};
};

void testPinballIntegration()
{
    testBanner("Integration Test (StateMachines)");

    FakePinball pinball;

   //LEDs should be accessed (attract mode):
    {
        for(int i = 0; i < 60; i++)
        {
            pinball.step();
        }

        int ledSpyCount = pinball.leds.getSpyCountSetRGB();
        std::cout << "LED access spy count: " << ledSpyCount << std::endl;

        if(!assert(pinball.leds.getSpyCountSetRGB() > 60, "LEDs should be accessed.")) goto footer;
    }

    //Once start button pressed, LEDs should not be accessed:
    {
        pinball.startButton.press();
        pinball.step();
        pinball.startButton.release();

        int ledSpyCount = pinball.leds.getSpyCountSetRGB();

        for(int i = 0; i < 60; i++)
        {
            pinball.step();
        }

        int newLedSpyCount = pinball.leds.getSpyCountSetRGB();

        
        if(!assertEqual(ledSpyCount, newLedSpyCount, "LEDs should not be accessed after start button pressed.")) goto footer;
    }

    return testFooter(true);

    footer:
        testFooter(false);
}