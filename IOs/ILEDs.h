#pragma once

class ILEDs
{
    public:
        virtual void setRGB(int index, int r, int g, int b) = 0;
        virtual void setHSV(int index, int h, int s, int v) = 0;
        virtual int getNbLEDs() const = 0;
        virtual void clearAll() = 0;
};