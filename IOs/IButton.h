#pragma once

class IButton
{
    public:

        virtual bool isPressed() = 0;
        virtual bool isPressedDebounced() = 0;
};