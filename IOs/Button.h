#pragma once

#include "IButton.h"
#include "../Utils/Utils.h"

enum class DebounceState
{
    RELEASED,
    PRESSED
};

class Button : public IButton
{
    public:

        Button(const int pin, const int gpioGndPin = -1) : pin(pin), gpioGndPin(gpioGndPin)
        {
            pinMode(pin, INPUT_PULLUP);

            if(gpioGndPin != -1)
            {
                pinMode(gpioGndPin, OUTPUT);
                digitalWrite(gpioGndPin, LOW);
            }
        }

        bool isPressed()
        {
            return digitalRead(pin) == LOW;
        }

        bool isPressedDebounced()
        {
            unsigned long currentTime = myMicros();

            switch(state)
            {
                case DebounceState::RELEASED:

                    if((currentTime - lastReleaseTime) < 50000)
                    {
                        break;
                    }

                    if(isPressed())
                    {
                        state = DebounceState::PRESSED;
                        lastPressTime = currentTime;
                        return true;
                    }
                    break;

                case DebounceState::PRESSED:
                    if((currentTime - lastPressTime) < 50000)
                    {
                        break;
                    }

                    if(!isPressed())
                    {
                        state = DebounceState::RELEASED;
                        lastReleaseTime = currentTime;
                        return false;
                    }

                    break;
            }

            return false;
        }

    private:

        DebounceState state = DebounceState::RELEASED;
        const int pin;
        const int gpioGndPin;
        unsigned long lastPressTime = 0;
        unsigned long lastReleaseTime = 0;
};