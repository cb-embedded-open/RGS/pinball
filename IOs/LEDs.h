#pragma once

#include "ILEDs.h"
#include <FastLED.h>

template <int NbLEDs, int DataPin>
class LEDs : public ILEDs
{

    public:

        LEDs()
        {
            FastLED.addLeds<NEOPIXEL, DataPin>(leds, NbLEDs);
            clearAll();
        }

        void setLED(int index, CRGB color)
        {
            leds[index] = color;
        }

        void setRGB(int index, int r, int g, int b) override
        {
            updated = true;
            leds[index] = CRGB(r, g, b);
        }

        void clearAll() override
        {
            for (int i = 0; i < NbLEDs; i++)
            {
                leds[i] = CRGB::Black;
            }
        }

        void setHSV(int index, int h, int s, int v) override
        {
            updated = true;
            leds[index] = CHSV(h, s, v);
        }

        int getNbLEDs() const override
        {
            return NbLEDs;
        }

        void show()
        {
            if(!updated)
                return;
            FastLED.show();
            updated = false;
        }

    private:
        bool updated = false;
        const int dataPin;
        CRGB leds[NbLEDs];
};