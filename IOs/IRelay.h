#pragma once

class IRelay
{
    public:

        virtual void on() = 0;
        virtual void off() = 0;
};