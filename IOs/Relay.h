#pragma once

#include "IRelay.h"
#include "../Utils/Utils.h"

class Relay : public IRelay
{
    public:

        Relay(const int pin) : pin(pin)
        {
            pinMode(pin, OUTPUT);
        }

        void on()
        {
            digitalWrite(pin, HIGH);
        }

        void off()
        {
            digitalWrite(pin, LOW);
        }

    private:
        const int pin;
};