#pragma once

#include "Utils.h"
#include "ISynchronizer.h"

template <bool Periodic>
class Synchronizer : public ISynchronizer, public IPeriodicSynchronizer, public IOneShotSynchronizer
{

public:

    Synchronizer(unsigned long periodUs = 20000) {
       
        setPeriodUs(periodUs);

        this->deadline = myMicros();
        this->running = false;

        if(Periodic)
        {
            trigger();
        }
    }

    bool sync()
    {
        if ((micros() < deadline) || !running)
            return false;

        if(Periodic)
        {
            deadline = myMicros() + periodUs;
        }
        else
        {
            running = false;
        }
        
        return true;
    }

    void trigger()
    {
        running = true;
        deadline = myMicros() + periodUs;
    }

    void stop()
    {
        running = false;
    }

    bool isRunning()
    {
        return running;
    }

    void setPeriodUs(unsigned long periodUs)
    {
        this->periodUs = periodUs;
    }

private:

    bool running;
    unsigned long periodUs;
    unsigned long deadline;
};

using PeriodicSynchronizer = Synchronizer<true>;
using OneShotSynchronizer = Synchronizer<false>;