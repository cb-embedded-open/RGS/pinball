#pragma once

#ifndef ARDUINO
#include <cstdint>
#endif

template <int N>
struct BCDBase
{
    BCDBase()
    {

    }

    BCDBase(int value)
    {
        m_bcd[N] = '\0';

        for(int i = N-1; i >= 0; i--)
        {
            m_bcd[i] = '0' + (value % 10);
            value /= 10;
        }
    }

    const char * c_str() const
    {
        return m_bcd;
    }

    template <int M>
    friend BCDBase<M> operator+(BCDBase<M> a, BCDBase<M> b);

    template <int M>
    friend BCDBase<M> operator-(BCDBase<M> a, BCDBase<M> b);

    template <int M>
    friend void operator+=(BCDBase<M> & a, BCDBase<M> b);

    template <int M>
    friend void operator-=(BCDBase<M> & a, BCDBase<M> b);

    template <int M>
    friend bool operator>(BCDBase<M> a, BCDBase<M> b);

    template <int M>
    friend bool operator<(BCDBase<M> a, BCDBase<M> b);

    template <int M>
    friend bool operator>=(BCDBase<M> a, BCDBase<M> b);

    template <int M>
    friend bool operator<=(BCDBase<M> a, BCDBase<M> b);

    template <int M>
    friend BCDBase<M> operator--(BCDBase<M> & a, int);

private: 
    char m_bcd[N+1];
};

template <int N>
BCDBase<N> operator+(BCDBase<N> a, BCDBase<N> b)
{
    BCDBase<N> result;
    result.m_bcd[N] = '\0';

    uint8_t carry = 0;

    for(int i = N-1; i >=0; i--)
    {
        char sum = a.m_bcd[i] + b.m_bcd[i] + carry - '0';
        
        if(sum > '9')
        {
            carry = 1;
            sum -= 10;
        }
        else
        {
            carry = 0;
        }

        result.m_bcd[i] = sum;
    }

    return result;
}

template <int N>
BCDBase<N> operator-(BCDBase<N> a, BCDBase<N> b)
{
    BCDBase<N> result;
    result.m_bcd[N] = '\0';

    uint8_t borrow = 0;

    for(int i = N-1; i >=0; i--)
    {
        char diff = a.m_bcd[i] - b.m_bcd[i] - borrow + '0';
        
        if(diff < '0')
        {
            borrow = 1;
            diff += 10;
        }
        else
        {
            borrow = 0;
        }

        result.m_bcd[i] = diff;
    }

    return result;
}

template <int N>
void operator+=(BCDBase<N> & a, BCDBase<N> b)
{
    a = a + b;
}

template <int N>
void operator-=(BCDBase<N> & a, BCDBase<N> b)
{
    a = a - b;
}

template <int N>
bool operator>(BCDBase<N> a, BCDBase<N> b)
{
    for(int i = 0; i < N; i++)
    {
        if(a.m_bcd[i] == b.m_bcd[i])
        {
            continue;
        }

        if(a.m_bcd[i] > b.m_bcd[i])
            return true;

        return false;
    }

    return false;
}

template <int N>
bool operator<(BCDBase<N> a, BCDBase<N> b)
{
    return b > a;
}

template <int N>
bool operator>=(BCDBase<N> a, BCDBase<N> b)
{
    return !(a < b);
}

template <int N>
bool operator<=(BCDBase<N> a, BCDBase<N> b)
{
    return !(a > b);
}

template <int N>
BCDBase<N> operator--(BCDBase<N> & a, int)
{
    BCDBase<N> copy = a;
    a -= BCDBase<N>(1);
    return copy;
}


using BCD2 = BCDBase<2>;
using BCD5 = BCDBase<5>;
using BCD4 = BCDBase<4>;

using BCD = BCD4;