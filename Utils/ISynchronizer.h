#pragma once

class ISynchronizer
{
    public:
        virtual bool sync() = 0;
        virtual void trigger() = 0;
        virtual void stop() = 0;
        virtual bool isRunning() = 0;
        virtual void setPeriodUs(unsigned long periodUs) = 0;
};

class IOneShotSynchronizer : public ISynchronizer
{
};

class IPeriodicSynchronizer : public ISynchronizer
{
};