import re
import os

# Mapping for analog pins
analog_pins = {
    'A0': '14',
    'A1': '15',
    'A2': '16',
    'A3': '17',
    'A4': '18',
    'A5': '19',
}

# Function to read the Pinout.h file and extract pin-name pairs
def parse_pinout_header(file_path):
    pin_mappings = {}
    with open(file_path, 'r') as file:
        for line in file:
            match = re.match(r'\w+ int (\w+) = (A\d+|\d+);', line)
            if match:
                pin_name, pin_number = match.groups()
                if pin_number in analog_pins:
                    pin_number = analog_pins[pin_number]
                pin_name = re.sub(r'Pin$', '', pin_name)  # Remove the 'Pin' suffix
                pin_name = pin_name[0].upper() + pin_name[1:]
                pin_mappings[pin_number] = pin_name

    return pin_mappings

# Function to replace placeholders in the SVG file and remove remaining placeholders
def replace_and_clean_placeholders_in_svg(template_path, output_path, pin_mappings):
    with open(template_path, 'r') as template_file:
        svg_content = template_file.read()

    # Replace placeholders
    for pin, name in pin_mappings.items():
        placeholder = f'PLACEHOLDER_D{pin}'
        svg_content = svg_content.replace(placeholder, name)

    # Remove remaining placeholders
    svg_content = re.sub(r'PLACEHOLDER_\w+', '', svg_content)

    with open(output_path, 'w') as output_file:
        output_file.write(svg_content)

# File paths relative to the calling directory
header_file_path = 'Pinout.h'  # Pinout.h at the root
svg_template_path = os.path.join('public', 'PinoutTemplate.svg')
svg_output_path = os.path.join('public', 'Pinout.svg')

# Read the .h file and extract pin mappings
pin_mappings = parse_pinout_header(header_file_path)

# Replace placeholders and clean the SVG file
replace_and_clean_placeholders_in_svg(svg_template_path, svg_output_path, pin_mappings)
