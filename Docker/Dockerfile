FROM ubuntu:latest

# Installer les dépendances nécessaires
RUN apt-get update && apt-get install -y \
    curl \
    python3 \
    python3-pip \
    build-essential \
    gcc-avr \
    avr-libc \
    git \
    cmake \
    libelf-dev \
    pkg-config

# Télécharger et compiler simavr
RUN git clone https://github.com/buserror/simavr.git /opt/simavr && \
    cd /opt/simavr/simavr && \
    make install RELEASE=1

# Installer arduino-cli
RUN curl -fsSL https://raw.githubusercontent.com/arduino/arduino-cli/master/install.sh | sh

# Ajouter arduino-cli au PATH
ENV PATH="/root/bin:${PATH}"

# Mettre à jour l'index des cores et installer le core AVR
RUN arduino-cli core update-index
RUN arduino-cli core install arduino:avr

# Installer les bibliothèques nécessaires
RUN arduino-cli lib install Mozzi
RUN arduino-cli lib install FastLED
RUN arduino-cli lib install "Simple LED Matrix"

# Définir le point d'entrée par défaut
CMD ["arduino-cli"]

