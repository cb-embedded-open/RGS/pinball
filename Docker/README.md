# To build push the container to the docker registry

```bash
docker build --progress=plain -t cbembedded/arduino-cli:latest .
docker push cbembedded/arduino-cli:latest
```