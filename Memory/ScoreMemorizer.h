#pragma once

#include "IScoreMemorizer.h"
#include <EEPROM.h>

class ScoreMemorizer : public IScoreMemorizer
{
    public:

        ScoreMemorizer()
        {
            uint32_t magicNumberRead;
            EEPROM.get(0, magicNumberRead);

            if(magicNumberRead != magicNumber)
            {
                EEPROM.put(0, magicNumber);
                setScore(BCD(0424));
                setInitials("CEDR");
            }
        }

        virtual void setScore(BCD score)
        {
            EEPROM.put(sizeof(magicNumber), score);
        }

        virtual BCD getScore()
        {
            BCD score;
            EEPROM.get(sizeof(magicNumber), score);
            return score;
        }

        virtual void setInitials(const char* initials)
        {
            char initialsBuffer[sizeof(initials)];
            strncpy(initialsBuffer, initials, sizeof(initials) - 1);
            initialsBuffer[sizeof(initials-1)] = '\0';

            EEPROM.put(sizeof(magicNumber) + sizeof(BCD), initialsBuffer);
        }

        virtual const char* getInitials()
        {
            EEPROM.get(sizeof(magicNumber) + sizeof(BCD), initials);
            return initials;
        }
    
    private:
        char initials[5];
        const uint32_t magicNumber = 0x12345678;
};