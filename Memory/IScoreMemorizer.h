#pragma once

#include "../Utils/BCD.h"

class IScoreMemorizer
{
public:
    virtual void setScore(BCD score) = 0;
    virtual BCD getScore() = 0;
    virtual void setInitials(const char* initials) = 0;
    virtual const char* getInitials() = 0;
};