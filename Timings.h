//Timings in microseconds (us):
const unsigned long beepDurationUs = 100000;
const unsigned long attractPeriodUs = 100000;
const unsigned long highScoreInitialsPeriodUs = 100000;
const unsigned long countdownPeriodUs = 1000000;