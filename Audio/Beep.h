#pragma once

#include "IBeep.h"

#include <MozziGuts.h>
#include <Oscil.h>
#include <tables/saw2048_int8.h>
#include <tables/sin2048_int8.h>
#include <tables/square_no_alias_2048_int8.h>
#include <tables/triangle2048_int8.h>


#include "../Utils/ISynchronizer.h"

class TriangleOsc
{
public:
    using Type = Oscil<TRIANGLE2048_NUM_CELLS, AUDIO_RATE>;
    static constexpr const int8_t * const oscData = TRIANGLE2048_DATA;
};

class SquareOsc
{
public:
    using Type = Oscil<SQUARE_NO_ALIAS_2048_NUM_CELLS, AUDIO_RATE>;
    static constexpr const int8_t * const oscData = SQUARE_NO_ALIAS_2048_DATA;
};

class SawOsc
{
    public:
        using Type = Oscil<SAW2048_NUM_CELLS, AUDIO_RATE>;
        static constexpr const int8_t * const oscData = SAW2048_DATA;
};

class SineOsc
{
    public:
        using Type = Oscil<SIN2048_NUM_CELLS, AUDIO_RATE>;
        static constexpr const int8_t * const oscData = SIN2048_DATA;
};


template <class OscType>
class Beep : public IBeep
{
    public:

        Beep(IOneShotSynchronizer & sync, const int freq) :
        freq(freq), osc(OscType::oscData), sync(sync)
        {
        }

        void beep(int freq)
        {
            sync.trigger();
            osc.setFreq(freq);
        }

        void beep()
        {
            beep(freq);
        }

        void step()
        {
            if(!sync.sync())
                return;
            
            osc.setFreq(0);
        }

    private:
        const int freq;
        IOneShotSynchronizer & sync;
        typename OscType::Type osc;

        friend int updateAudio();
};

using SquareBeep = Beep<SquareOsc>;