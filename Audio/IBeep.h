#pragma once

class IBeep
{
    public:
        virtual void beep() = 0;
        virtual void beep(int freq) = 0;
        virtual void step() = 0;
};