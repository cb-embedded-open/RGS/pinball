class IMusic
{
    public:
        virtual void play() = 0;
        virtual void stop() = 0;
};