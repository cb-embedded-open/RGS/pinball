import wave
import numpy as np
import sys

def convert_wav_to_header(wav_path, output_header_path):
    """
    Convert a WAV file to a C header file.

    Args:
        wav_path (str): The path to the input WAV file.
        output_header_path (str): The path to the output C header file.
        soundname (str): The name of the sound.

    Returns:
        None
    """

    soundname = wav_path.split('/')[-1].replace('.wav', '')

    with wave.open(wav_path, 'r') as wav_file:
        # Get the parameters of the WAV file
        nchannels, sampwidth, framerate, nframes, comptype, compname = wav_file.getparams()

        # Read the frames
        frames = wav_file.readframes(nframes)
        
        # Adjust buffer size if necessary
        buffer_length = len(frames)
        sample_size = nchannels * sampwidth
        if buffer_length % sample_size != 0:
            frames = frames[:-(buffer_length % sample_size)]

    # Convert bytes to numpy array of type int16
    data = np.frombuffer(frames, dtype=np.int16)

    # Ensure the data is mono
    if nchannels > 1:
        data = data[0::nchannels]  # Take one channel

    # Normalize to int8
    data_normalized = np.int8((data / 2**15) * 127)

    # Generate the content of the header file
    header_content = f"""#ifndef {soundname.upper()}_H_
#define {soundname.upper()}_H_

#if ARDUINO >= 100
 #include "Arduino.h"
#else
 #include "WProgram.h"
#endif
#include "mozzi_pgmspace.h"

#define {soundname.upper()}_NUM_CELLS {len(data_normalized)}
#define {soundname.upper()}_SAMPLERATE {framerate}

CONSTTABLE_STORAGE(int8_t) {soundname.upper()}_DATA [] = {{
"""

    # Add the data to the array
    data_str = ", ".join(map(str, data_normalized))
    header_content += data_str + "\n};\n\n#endif /* " + soundname.upper() + "_H_ */"

    # Write to the header file
    with open(output_header_path, 'w') as f:
        f.write(header_content)

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: python script.py input.wav output.h")
        sys.exit(1)
    convert_wav_to_header(sys.argv[1], sys.argv[2])
