import mido
import sys

def parse_midi_file(input_file, output_file):
    # Load the midi file
    mid = mido.MidiFile(input_file)

    events = []

    ms_per_tick = mido.tick2second(1, mid.ticks_per_beat, 500000)*1000

    # Iterate through the midi file
    for i, track in enumerate(mid.tracks):
        absoluteTime = 0

        for msg in track:
            absoluteTime += msg.time

            if msg.type == 'note_on' or msg.type == 'note_off':
                if msg.channel == 9:
                    continue

                off = (msg.type == 'note_off') or (msg.velocity == 0)
                note = msg.note if not off else 0

                events.append([absoluteTime, msg.channel, note])

    events.sort()

    # List channels
    channels = set([event[1] for event in events])
    # Attribute a number to each channel
    channelDict = {channel: i for i, channel in enumerate(channels)}

    # Create a C header file with the events (4 bytes)
    with open(output_file, 'w') as f:

        name = input_file.split('/')[-1].replace('.mid', '')

        f.write(f"""
#ifndef {name.upper()}_H_
#define {name.upper()}_H_

#if ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif
#include "mozzi_pgmspace.h"

#define {name.upper()}_NUM_CELLS {len(events)}
#define {name.upper()}_MS_PER_KILO_TICKS {int(ms_per_tick*1000)}

CONSTTABLE_STORAGE(uint32_t) {name.upper()}_DATA [] = {{
""")
        
        prevTime = 0
        events_text = ""
        for event in events:
            deltaTime = round((event[0] - prevTime)/2)
            prevTime = event[0]
            print(f"Delta={deltaTime} ticks\tChannelID={channelDict[event[1]]}\tNote={event[2]}\t0x{deltaTime:04x}{channelDict[event[1]]:02x}{event[2]:02x}")
            events_text += f"    0x{deltaTime:04x}{channelDict[event[1]]:02x}{event[2]:02x},\n"

        f.write(f"""{events_text}}};

#endif /* {name.upper()}_H_ */
""")
        print("Used memory:", len(events)*4,"bytes")
        print("ms per tick:", ms_per_tick)


def main():
    if len(sys.argv) != 3:
        print("Usage: python MIDIDisplay.py <input_file> <output_file>")
        return

    input_file = sys.argv[1]
    output_file = sys.argv[2]

    parse_midi_file(input_file, output_file)

if __name__ == "__main__":
    main()
