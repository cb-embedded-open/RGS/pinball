#!/bin/bash

# WaveFiles: Files are in ../WAV and will be converted and saved in ../Samples
for file in ../WAV/*.wav; do
    echo "Converting $file"
    python convertWavToHeader.py $file ../Samples/$(basename $file .wav).h $(basename $file .wav)
done

# MIDIFiles: Files are in ../MIDI and will be converted and saved in ../Music
for file in ../MIDI/*.mid; do
    echo "Converting $file"
    python convertMIDIToHeader.py $file ../Music/$(basename $file .mid).h
done