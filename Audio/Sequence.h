#include <mozzi_pgmspace.h>

struct Event
{
  uint16_t delta;
  int8_t channel;
  int8_t midiNote;

  Event(uint32_t data)
  {
    delta = (data >> 16) & 0xFF;
    channel = (data >> 8) & 0xFF;
    midiNote = data & 0xFF;
  }
};

template <class Osc0Type, class Osc1Type, class Osc2Type>
class Sequence {

  public:

  Sequence(const uint32_t * data, const int length, Osc0Type & osc0, Osc1Type & osc1, Osc2Type & osc2)
  : osc0(osc0), osc1(osc1), osc2(osc2), data(data), length(length)
  {
  }

  void init()
  {
    if (length == 0)
      return;

    Event e = FLASH_OR_RAM_READ<const uint32_t>(&data[0]);

    absoluteDeadline = millis() + e.delta;
  }

  void playEvent(Event & e)
  {
    int freq = e.midiNote ? mtof(e.midiNote) : 0;

    switch(e.channel)
    {
      case 0:
        osc0.setFreq(freq);
        break;
      case 1:
        osc1.setFreq(freq);
        break;
      case 2:
        osc2.setFreq(freq);
        break;
    }
  }

  void update()
  {
    while(step());
  }

  private:

    bool step()
    {
      unsigned long currentMillis = millis();

      if(currentMillis < absoluteDeadline)
        return false;
    
      Event e = FLASH_OR_RAM_READ<const uint32_t>(&data[pos]);

      playEvent(e);
      pos++;
      
      if (pos >= length)
      {
        pos = 0;
      }

      e = FLASH_OR_RAM_READ<const uint32_t>(&data[pos]);
      absoluteDeadline = millis() + e.delta;

      return true;
    }

    const uint32_t * data;
    unsigned length;
    int pos = 0;
    unsigned long absoluteDeadline = 0;

    Osc0Type & osc0;
    Osc1Type & osc1;
    Osc2Type & osc2;
};