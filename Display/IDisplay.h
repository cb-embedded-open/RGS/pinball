#pragma once

class IDisplay
{
    public:

        virtual void print(const char* txt) = 0;
        virtual void print(const char* txt, int16_t offset) = 0;
};