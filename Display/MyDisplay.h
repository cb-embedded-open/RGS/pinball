#pragma once
#include <Arduino.h>
#include <SPI.h>
#include "IDisplay.h"

static const uint8_t font8x8[][8] PROGMEM =
    {
        // Space
        {0b0000000,
         0b0000000,
         0b0000000,
         0b0000000,
         0b0000000,
         0b0000000,
         0b0000000,
         0b0000000},

        // A
        {0b0111110,
         0b1100011,
         0b1100011,
         0b1111111,
         0b1100011,
         0b1100011,
         0b1100011,
         0b1100011},

        // B
        {0b1111110,
         0b1100011,
         0b1100011,
         0b1111110,
         0b1100011,
         0b1100011,
         0b1100011,
         0b1111110},

        // C
        {0b0111110,
         0b1100011,
         0b1100000,
         0b1100000,
         0b1100000,
         0b1100000,
         0b1100011,
         0b0111110},

        // D
        {0b1111100,
         0b1100110,
         0b1100011,
         0b1100011,
         0b1100011,
         0b1100011,
         0b1100110,
         0b1111100},

        // E
        {0b1111111,
         0b1100000,
         0b1100000,
         0b1111100,
         0b1100000,
         0b1100000,
         0b1100000,
         0b1111111},

        // F
        {0b1111111,
         0b1100000,
         0b1100000,
         0b1111100,
         0b1100000,
         0b1100000,
         0b1100000,
         0b1100000},

        // G
        {0b0111110,
         0b1100011,
         0b1100000,
         0b1100000,
         0b1100111,
         0b1100011,
         0b1100011,
         0b0111110},

        // H
        {0b1100011,
         0b1100011,
         0b1100011,
         0b1111111,
         0b1100011,
         0b1100011,
         0b1100011,
         0b1100011},

        // I
        {0b011110,
         0b001100,
         0b001100,
         0b001100,
         0b001100,
         0b001100,
         0b001100,
         0b011110},

        // J
        {0b0000111,
         0b0000110,
         0b0000110,
         0b0000110,
         0b0000110,
         0b1100110,
         0b1100110,
         0b0111000},

        // K
        {0b1100011,
         0b1100110,
         0b1101100,
         0b1111000,
         0b1111000,
         0b1101100,
         0b1100110,
         0b1100011},

        // L
        {0b1100000,
         0b1100000,
         0b1100000,
         0b1100000,
         0b1100000,
         0b1100000,
         0b1100000,
         0b1111111},

        // M
        {0b1100011,
         0b1110111,
         0b1111111,
         0b1101011,
         0b1100011,
         0b1100011,
         0b1100011,
         0b1100011},

        // N
        {0b1100011,
         0b1110011,
         0b1111011,
         0b1101111,
         0b1100111,
         0b1100011,
         0b1100011,
         0b1100011},

        // O
        {0b0111110,
         0b1100011,
         0b1100011,
         0b1100011,
         0b1100011,
         0b1100011,
         0b1100011,
         0b0111110},

        // P
        {0b1111110,
         0b1100011,
         0b1100011,
         0b1111110,
         0b1100000,
         0b1100000,
         0b1100000,
         0b1100000},

        // Q
        {0b0111110,
         0b1100011,
         0b1100011,
         0b1100011,
         0b1100011,
         0b1100111,
         0b1100011,
         0b0111111},

        // R
        {0b1111110,
         0b1100011,
         0b1100011,
         0b1111110,
         0b1111000,
         0b1101100,
         0b1100110,
         0b1100011},

        // S
        {0b0111110,
         0b1100011,
         0b1100000,
         0b0111110,
         0b0000011,
         0b0000011,
         0b1100011,
         0b0111110},

        // T
        {0b1111111,
         0b0011000,
         0b0011000,
         0b0011000,
         0b0011000,
         0b0011000,
         0b0011000,
         0b0011000},

        // U
        {0b1100011,
         0b1100011,
         0b1100011,
         0b1100011,
         0b1100011,
         0b1100011,
         0b1100011,
         0b0111110},

        // V
        {0b1100011,
         0b1100011,
         0b1100011,
         0b1100011,
         0b1100011,
         0b1100011,
         0b0110110,
         0b0011100},

        // W
        {0b1100011,
         0b1100011,
         0b1100011,
         0b1100011,
         0b1101011,
         0b1111111,
         0b1110111,
         0b1100011},

        // X
        {0b1100011,
         0b1100011,
         0b0110110,
         0b0011100,
         0b0011100,
         0b0110110,
         0b1100011,
         0b1100011},

        // Y
        {0b1100011,
         0b1100011,
         0b0110110,
         0b0011100,
         0b0011000,
         0b0011000,
         0b0011000,
         0b0011000},

        // Z
        {0b1111111,
         0b0000011,
         0b0000110,
         0b0001100,
         0b0011000,
         0b0110000,
         0b1100000,
         0b1111111},

        // Digit 0
        {0b0111110,
         0b1100011,
         0b1100111,
         0b1101011,
         0b1110011,
         0b1100011,
         0b1100011,
         0b0111110},

        // Digit 1
        {0b0011000,
         0b0111000,
         0b0011000,
         0b0011000,
         0b0011000,
         0b0011000,
         0b0011000,
         0b1111111},

        // Digit 2
        {0b0111110,
         0b1100011,
         0b0000011,
         0b0000110,
         0b0001100,
         0b0011000,
         0b0110000,
         0b1111111},

        // Digit 3
        {0b0111110,
         0b1100011,
         0b0000011,
         0b0001110,
         0b0000011,
         0b0000011,
         0b1100011,
         0b0111110},

        // Digit 4
        {0b0001110,
         0b0011110,
         0b0110110,
         0b1100110,
         0b1111111,
         0b0000110,
         0b0000110,
         0b0000110},

        // Digit 5
        {0b1111111,
         0b1100000,
         0b1100000,
         0b1111110,
         0b0000011,
         0b0000011,
         0b1100011,
         0b0111110},

        // Digit 6
        {0b0111110,
         0b1100011,
         0b1100000,
         0b1111110,
         0b1100011,
         0b1100011,
         0b1100011,
         0b0111110},

        // Digit 7
        {0b1111111,
         0b0000011,
         0b0000110,
         0b0001100,
         0b0011000,
         0b0011000,
         0b0011000,
         0b0011000},

        // Digit 8
        {0b0111110,
         0b1100011,
         0b1100011,
         0b0111110,
         0b1100011,
         0b1100011,
         0b1100011,
         0b0111110},

        // Digit 9
        {0b0111110,
         0b1100011,
         0b1100011,
         0b1100011,
         0b0111111,
         0b0000011,
         0b0000110,
         0b0111100}};

static const uint8_t getFontRow(char c, uint8_t row)
{
    if (c >= 'A' && c <= 'Z')
        c = c - 'A' + 1;
    else if (c >= '0' && c <= '9')
        c = c - '0' + 27;
    else
        c = 0;
    return pgm_read_byte(&(font8x8[c][row]));
}

class MyDisplay : public IDisplay
{
public:
    MyDisplay(uint8_t csPin, uint8_t numMatrices);
    void print(const char *str);
    void print(const char *str, int16_t offset);

private:
    uint8_t _csPin;
    uint8_t _numMatrices;
    void begin();
    void sendByte(uint8_t reg, uint8_t data);
    void printRow(uint8_t row, const char *str);
    void printRow(uint8_t row, const char *str, int16_t offset);
    void clearDisplay();
};

MyDisplay::MyDisplay(uint8_t csPin, uint8_t numMatrices)
    : _csPin(csPin), _numMatrices(numMatrices)
{
    begin();
}

void MyDisplay::begin()
{
    pinMode(_csPin, OUTPUT);

    // Begin with SPI Speed 4MHz
    SPI.begin();
    SPI.beginTransaction(SPISettings(4000000, MSBFIRST, SPI_MODE0));

    // Initialize MAX7219 registers
    sendByte(0x09, 0x00); // No decoding
    sendByte(0x0A, 0x0F); // Brightness
    sendByte(0x0B, 0x07); // Scan limit = 8 LEDs
    sendByte(0x0C, 0x01); // Normal operation mode
    sendByte(0x0F, 0x00); // Display test off
    clearDisplay();
}

void MyDisplay::sendByte(uint8_t reg, uint8_t data)
{
    digitalWrite(_csPin, LOW);
    for (uint8_t i = 0; i < _numMatrices; i++)
    {
        SPI.transfer(reg);
        SPI.transfer(data);
    }
    digitalWrite(_csPin, HIGH);
}

void MyDisplay::clearDisplay()
{
    for (uint8_t row = 0; row < 8; row++)
    {
        sendByte(row + 1, 0x00);
    }
}

void MyDisplay::printRow(uint8_t row, const char *str)
{
    digitalWrite(_csPin, LOW);
    for (uint8_t i = 0; i < _numMatrices; i++)
    {
        char c = str[i];
        SPI.transfer(row + 1);
        SPI.transfer(getFontRow(c, row));
    }
    digitalWrite(_csPin, HIGH);
}

void MyDisplay::printRow(uint8_t row, const char *str, int16_t offset)
{
    digitalWrite(_csPin, LOW);

    int16_t charIndex = (offset / 8);
    int16_t colOffset = offset % 8;
    int16_t strLen = strlen(str);

    if(offset < 0)
    {
        charIndex -= 1;
        colOffset += 8;
    }

    for (uint8_t i = 0; i < _numMatrices; i++)
    {
        char c;

        if((charIndex < strLen) && (charIndex >= 0))
            c = str[charIndex];
        else 
            c = 0;

        uint8_t fontData = getFontRow(c, row) << colOffset;

        if(((charIndex+1) < strLen) && ((charIndex +1) >= 0))
            c = str[charIndex+1];
        else
            c = 0;

        fontData |= getFontRow(c, row) >> (8 - colOffset);

        SPI.transfer(row + 1);
        SPI.transfer(fontData);

        charIndex++;
    }

    digitalWrite(_csPin, HIGH);
}


void MyDisplay::print(const char *str)
{
    for (uint8_t row = 0; row < 8; row++)
    {
        printRow(row, str);
    }
}

void MyDisplay::print(const char *str, int16_t offset)
{
    for (uint8_t row = 0; row < 8; row++)
    {
        printRow(row, str, offset);
    }
}
