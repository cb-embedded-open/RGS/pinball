//Peripherals
#include "IOs/Button.h"
#include "Display/IDisplay.h"
#include "Display/MyDisplay.h"
#include "IOs/Relay.h"
#include "Audio/Beep.h"

//Indicators
#include "IOs/LEDs.h"
#include "Indicators/TargetIndicators.h"
#include "Indicators/BonusIndicators.h"

//State Machines
#include "StateMachines/Targets.h"
#include "StateMachines/GamePlay.h"
#include "StateMachines/Countdown.h"
#include "StateMachines/HighScoreInitials.h"
#include "StateMachines/AttractMode.h"

//Tools
#include "Utils/Synchronizer.h"
#include "Utils/ISynchronizer.h"

//Memory
#include "Memory/ScoreMemorizer.h"

//Constants:
#include "Pinout.h"
#include "Timings.h"
#include "Contants.h"

class Pinball
{
  public:

  Pinball() :

  //Relay
  relay(relayPin),
  
  //Buttons & targets
  startButton(startButtonPin),
  actionButton(actionButtonPin),
  
  target1Button(target1ButtonPin),
  target2Button(target2ButtonPin),
  target3Button(target3ButtonPin),
  target4Button(target4ButtonPin),
  target5Button(target5ButtonPin),
  targetButtons{&target1Button, &target2Button, &target3Button, &target4Button, &target5Button},
  
  //Timings (in us)
  beepSync(beepDurationUs),
  attractSync(attractPeriodUs),
  countdownSync(countdownPeriodUs),
  highScoreInitialsSync(highScoreInitialsPeriodUs),
  
  //Displays
  leds(),
  scoreDisplay(scoreDisplayCSPin, 4),
  initialsDisplay(initialsDisplayCSPin, 4),
  countdownDisplay(countdownDisplayCSPin, 2),
  highScoreDisplay(highScoreDisplayCSPin, 4),
  beep(beepSync, 440),

  //Indicators
  targetIndicators(5, leds),
  bonusIndicators(3, leds),

  //State machines
  targets(targetButtons, beep, scoreDisplay, targetIndicators, bonusIndicators),
  highscore(5678),
  score(4321),
  initials("AAA"),
  attractMode(
                leds,
                attractSync,
                countdownDisplay,
                initialsDisplay,
                scoreDisplay,
                highScoreDisplay,
                highscore,
                score,
                initials
              ),
  highScoreInitials(startButton, actionButton, initialsDisplay, highScoreInitialsSync),
  countdown(BCD4(60), countdownSync),
  //Memory:
  scoreMemorizer(),
  //Main State machine:
  gamePlay( startButton,
            relay,
            attractMode,
            countdown,
            highScoreInitials,
            countdownDisplay,
            targets,
            scoreMemorizer
        )
  {
  }

  void step()
  {
    gamePlay.step();
    leds.show();
    beep.step();
  }

  private:

    //IOs.
    Button startButton;
    Button actionButton;
    Button target1Button;
    Button target2Button;
    Button target3Button;
    Button target4Button;
    Button target5Button;
    IButton * targetButtons[5];
    Relay relay;

    //Displays.
    MyDisplay scoreDisplay;
    MyDisplay initialsDisplay;
    MyDisplay countdownDisplay;
    MyDisplay highScoreDisplay;
    
    LEDs<nbLEDs, ledDataPin> leds;

    //Indicators.
    TargetIndicators targetIndicators;
    BonusIndicators bonusIndicators;

    //State machines.
    Targets targets;
    AttractMode attractMode;
    GamePlay gamePlay;
    HighScoreInitials highScoreInitials;
    Countdown countdown;

    //Audio.
    SquareBeep beep;
    
    //Synchronizers:
    OneShotSynchronizer beepSync;
    PeriodicSynchronizer attractSync;
    PeriodicSynchronizer countdownSync;
    PeriodicSynchronizer highScoreInitialsSync;

    //Memory
    ScoreMemorizer scoreMemorizer;

    //Score
    BCD highscore;
    BCD score;
    char * initials;

    friend int updateAudio();
};