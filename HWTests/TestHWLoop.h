#include "../IOs/LEDs.h"
#include "../Display/Display.h"
#include <MozziGuts.h>
#include <Oscil.h>
#include <tables/sin2048_int8.h>

#include "../Pinout.h"

Oscil<SIN2048_NUM_CELLS, AUDIO_RATE> sinOsc(SIN2048_DATA);

class TestContext
{
    public:

    TestContext()
    :
    scoreDisplay(scoreDisplayCSPin, 4),
    initialsDisplay(initialsDisplayCSPin, 4),
    countdownDisplay(countdownDisplayCSPin, 2),
    highScoreDisplay(highScoreDisplayCSPin, 4),
    leds()
    {
        startMozzi();
        sinOsc.setFreq(440);
        leds.clearAll();
    }

    void loop()
    {
        for(int i=0;i<256;i++)
            audioHook();

        scoreDisplay.print("1234");
        initialsDisplay.print("ABCD");
        countdownDisplay.print("12");
        highScoreDisplay.print("5678");

        for(int i=0;i<18;i++)
        {
            leds.setRGB(i, 255, 0, 0);
        }

        leds.show();
    }

    Display scoreDisplay;
    Display initialsDisplay;
    Display countdownDisplay;
    Display highScoreDisplay;
    LEDs<18, ledDataPin> leds;
};

int updateAudio()
{
    return sinOsc.next();
}

void updateControl()
{
}

void testHwLoop()
{
    TestContext context;

    while(true)
    {
        context.loop();
    }
}

void setup()
{
    testHwLoop();
}

void loop()
{
}