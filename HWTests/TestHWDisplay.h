#include "../Display/MyDisplay.h"
#include "../Pinout.h"

void testHWDisplay()
{
    MyDisplay scoreDisplay(scoreDisplayCSPin, 4);

    for(int i=0; i<10; i++)
    {
        scoreDisplay.print("1234");
        delay(10);
    }

    MyDisplay initialsDisplay(initialsDisplayCSPin, 4);

    for(int i=0; i<10; i++)
    {
        initialsDisplay.print("ABCD");
        delay(10);
    }

    MyDisplay countdownDisplay(countdownDisplayCSPin, 2);

    for(int i=0; i<10; i++)
    {
        countdownDisplay.print("12");
        delay(10);
    }

    MyDisplay highScoreDisplay(highScoreDisplayCSPin, 4);

    for(int i=0; i<10; i++)
    {
        highScoreDisplay.print("5678");
        delay(10);
    }
}