#pragma once
#include "../Pinball.h"

#include <MozziGuts.h>
#include <Oscil.h>
#include <tables/sin2048_int8.h>

#include <new>

Pinball pinball;

void setup()
{
  startMozzi();
  adcReconnectAllDigitalIns();

  new (&pinball) Pinball();
}

void updateControl()
{
  pinball.step();
}


void loop()
{
  for(int i=0;i<256;i++)
    audioHook();

  updateControl();
}


int updateAudio()
{
  return pinball.beep.osc.next();
}