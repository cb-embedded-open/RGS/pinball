#include "../Utils/Synchronizer.h"

void setup()
{
    Serial.begin(115200);
    Serial.println("Test Synchronizers");

    OneShotSynchronizer oneShotSynchronizer((unsigned long)100000);
    PeriodicSynchronizer periodicSynchronizer((unsigned long)100000);

    int i = 0;

    oneShotSynchronizer.trigger();

    while(!oneShotSynchronizer.sync());

    Serial.println("One Shot Synchronizer");

    while(1)
    {
        if(periodicSynchronizer.sync())
        {
            i++;
            Serial.println("Periodic Synchronizer");
            Serial.println(i);
        }
    }
}