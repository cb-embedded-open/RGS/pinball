#include "../IOs/LEDs.h"
#include "../Pinout.h"

void testHWLEDs()
{
    LEDs<8, ledDataPin> leds;

    leds.clearAll();

    for(int i=0;i<8;i++)
    {
        leds.setRGB(i, 255, 0, 0);
        leds.show();
        delay(10);
        leds.setRGB(i, 0, 255, 0);
        leds.show();
        delay(10);
        leds.setRGB(i, 0, 0, 255);
        leds.show();
        delay(10);
    }
}