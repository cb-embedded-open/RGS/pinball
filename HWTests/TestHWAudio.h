#include <MozziGuts.h>
#include <Oscil.h>
#include <tables/sin2048_int8.h>

Oscil<SIN2048_NUM_CELLS, AUDIO_RATE> sinOsc(SIN2048_DATA);

void testMozzi()
{
    startMozzi();

    sinOsc.setFreq(440);

    unsigned long startTime = mozziMicros();
    while ((mozziMicros() - startTime) < 100000)
    {
        audioHook();
    }

    stopMozzi();
}

int updateAudio()
{
    return sinOsc.next();
}

void updateControl()
{
}