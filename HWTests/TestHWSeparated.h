# pragma once

#include "TestHWLEDs.h"
#include "TestHWDisplay.h"
#include "TestHWAudio.h"

void setup()
{
    testHWLEDs();
    testHWDisplay();
    testMozzi();

    asm volatile ("break");
}

void loop()
{
}