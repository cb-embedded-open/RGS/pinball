#pragma once

#include "../Display/MyDisplay.h"
//#include "../Display/Display.h"
#include "../Pinout.h"

void testHWMyDisplay()
{
    MyDisplay scoreDisplay(scoreDisplayCSPin, 4);
    delay(50);

    //Cycle through all characters;
    char str[] = {'A', 'B', 'C', 'D','\0'};

    while(1)
    {
        for(int i = 0; i < 26; i++)
        {
            str[0] = i+'A';
            str[1] = str[0];
            str[2] = str[0];
            str[3] = str[0];

            scoreDisplay.print(str);
            delay(200);
        }

        for(int i=0; i<10; i++)
        {
            str[0] = i+'0';
            str[1] = str[0];
            str[2] = str[0];
            str[3] = str[0];

            scoreDisplay.print(str);
            delay(500);
        }
    }
}

void testBareSPISpeed()
{
    //Test SPI using arduino SPI library
    SPI.begin();
    SPI.beginTransaction(SPISettings(4000000, MSBFIRST, SPI_MODE0));

    while(1)
    {
        digitalWrite(scoreDisplayCSPin, LOW);
        SPI.transfer(0x00);
        digitalWrite(scoreDisplayCSPin, HIGH);
    }
}

void loop()
{

}

void setup()
{
    testHWMyDisplay();
    //testBareSPISpeed();
}
