#include "../IOs/ILEDs.h"
#include "IIndicators.h"

class TargetIndicators : public IIndicators
{
    public:

        TargetIndicators(const int nbTargets, ILEDs & leds) : leds(leds), nbTargets(nbTargets)
        {
        }

        void set(int index)
        {
            if(!checkIndex(index))
                return;

            leds.setRGB(index + nbTargets*0, 255, 0, 0); // Indicator
            leds.setRGB(index + nbTargets*1, 255, 0, 0); // Spotlight 1
            leds.setRGB(index + nbTargets*2, 255, 0, 0); // Spotlight 2
        }

        void clear(int index)
        {
            if(!checkIndex(index))
                return;
                
            leds.setRGB(index + nbTargets*0, 0, 0, 0); // Indicator
            leds.setRGB(index + nbTargets*1, 0, 0, 0); // Spotlight 1
            leds.setRGB(index + nbTargets*2, 0, 0, 0); // Spotlight 2
        }

        void clearAll()
        {
            for(int i=0;i<nbTargets;i++)
            {
                leds.setRGB(i + nbTargets*0, 0, 0, 0); // Indicator
                leds.setRGB(i + nbTargets*1, 0, 0, 0); // Spotlight 1
                leds.setRGB(i + nbTargets*2, 0, 0, 0); // Spotlight 2
            }
        }

    private:

        bool checkIndex(int index)
        {
            return index >= 0 && index < nbTargets;
        }

        ILEDs & leds;
        const int nbTargets = 5;
};