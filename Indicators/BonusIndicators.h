#include "../IOs/ILEDs.h"
#include "IIndicators.h"

class BonusIndicators : public IIndicators
{
    public:
        BonusIndicators(const int nbIndicators, ILEDs & leds) : leds(leds), nbIndicators(nbIndicators)
        {
        }

        void set(int index)
        {
            leds.setRGB(offset+index, 255, 0, 0);
        }

        void clear(int index)
        {
            leds.setRGB(offset+index, 0, 0, 0);
        }

        void clearAll()
        {
            for(int i=0;i<nbIndicators;i++)
            {
                leds.setRGB(offset+i, 0, 0, 0);
            }
        }

    private:

        bool checkIndex(int index)
        {
            return index >= offset && index < nbIndicators;
        }

        ILEDs & leds;
        const int nbIndicators = 3;
        const int offset = 15;
};