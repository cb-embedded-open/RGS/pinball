#pragma once

class IIndicators
{
    public:
        virtual void set(int index) = 0;
        virtual void clear(int index) = 0;
        virtual void clearAll() = 0;
};